import androidx.compose.desktop.Window
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import com.cacaosd.disksize.widget.PieChart
import com.cacaosd.disksize.widget.PieChartData
import com.cacaosd.disksize.widget.PieChartData.Slice
import com.cacaosd.disksize.widget.renderer.SimpleSliceDrawer

fun main() = Window(size = IntSize(600, 480)) {
    var text by remember { mutableStateOf("Hello, World!") }

    MaterialTheme {
        Column {
            TopAppBar(title = {
                Text("Top Bar")
            })
            PieChart(
                pieChartData = PieChartData(
                    listOf(
                        Slice(60F, Color.Blue),
                        Slice(60F, Color.Cyan),
                        Slice(60F, Color.Green)
                    )
                ),
                // Optional properties.
                modifier = Modifier.size(50.dp),
                sliceDrawer = SimpleSliceDrawer()
            )
            Row(modifier = Modifier.fillMaxWidth().padding(16.dp)) {
                Column {


                    Button(onClick = {
                        text = "Hello, Desktop!"
                    }) {
                        Column {
                            Text(text)
                            Text(text)
                        }

                    }

                    Surface(
                        modifier = Modifier.size(50.dp),
                        shape = CircleShape,
                        color = MaterialTheme.colors.onSurface.copy(alpha = 0.2f)
                    ) {
                        // Image goes here
                    }

                    CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                        Text("3 minutes ago", style = MaterialTheme.typography.body2)
                    }



                    Button(onClick = {
                        text = "Hello, Desktop!"
                    }) {
                        Column {
                            Text(text)
                            Text(text)
                        }

                    }
                }

                Column {
                    cagdas()
                    cagdas()

                    Checkbox(checked = true, onCheckedChange = {

                    }, modifier = Modifier.align(Alignment.CenterHorizontally))

                }

                Column {
                    LazyColumn {
                        items(5) { item ->
                            Text("Item: $item")
                        }
                    }
                }


            }

            LazyList()
        }

    }

}

@Composable
fun LazyList() {
    // We save the scrolling position with this state that can also
    // be used to programmatically scroll the list
    val scrollState = rememberLazyListState()

    LazyColumn(state = scrollState) {
        items(100) {
            Text("Item #$it")
        }
    }
}


@Composable
fun cagdas() {
    FloatingActionButton(onClick = {
        print("click")
    }) {
        Text("FAB")
    }
}