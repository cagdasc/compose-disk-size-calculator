package com.cacaosd.disksize

import androidx.compose.desktop.Window
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import com.cacaosd.disksize.repository.FileDataViewModel
import com.cacaosd.disksize.repository.FileSizeRepository
import com.cacaosd.disksize.ui.SizeView
import kotlin.io.path.ExperimentalPathApi

@ExperimentalPathApi
@ExperimentalFoundationApi
fun main() {
    Window(
        title = "Disk Size Calculator",
        size = IntSize(700, 560)
    ) {
        val fileDataViewModel = remember { FileDataViewModel(FileSizeRepository()) }
        val rootFileData = fileDataViewModel.currentFile

        MaterialTheme {
            Column {
                TopAppBar(
                    title = { Text("Disk Size Calculator") },
                    navigationIcon = if (fileDataViewModel.isPreviousFileExist()) {
                        {
                            IconButton(onClick = {
                                fileDataViewModel.backPreviousFile()
                            }, modifier = Modifier.fillMaxWidth()) {
                                Icon(
                                    imageVector = Icons.Default.ArrowBack,
                                    contentDescription = "Back"
                                )
                            }
                        }
                    } else {
                        null
                    }
                )
                Box(modifier = Modifier.fillMaxSize().background(color = Color.LightGray).padding(16.dp)) {
                    Column(
                        modifier = Modifier.fillMaxSize().background(color = Color.Cyan),
                        verticalArrangement = Arrangement.Top,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        SizeView(rootFileData.value)
                        Divider()
                        rootFileData.value.children?.let { children ->
                            LazyVerticalGrid(
                                modifier = Modifier.align(Alignment.CenterHorizontally),
                                cells = GridCells.Adaptive(minSize = 128.dp)
                            ) {
                                items(children) { fileData ->
                                    SizeView(fileData) {
                                        fileDataViewModel.addNextFile(fileData)
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}
