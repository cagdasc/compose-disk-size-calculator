package com.cacaosd.disksize.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.cacaosd.disksize.repository.FileData
import com.cacaosd.disksize.widget.PieChart
import com.cacaosd.disksize.widget.PieChartData
import com.cacaosd.disksize.widget.renderer.SimpleSliceDrawer
import kotlin.io.path.ExperimentalPathApi
import kotlin.random.Random

@ExperimentalPathApi
@Composable
fun SizeView(fileSizeData: FileData, onClick: (() -> Unit)? = null) {
    val slices = createSlices(fileSizeData)
    Column(modifier = Modifier.clickable {
        onClick?.invoke()
    }, horizontalAlignment = Alignment.CenterHorizontally) {
        Text(text = fileSizeData.name, fontFamily = FontFamily.Monospace, fontWeight = FontWeight.Bold)
        PieChart(
            pieChartData = PieChartData(slices),
            modifier = Modifier.size(80.dp),
            sliceDrawer = SimpleSliceDrawer(40f)
        )

        Text(
            "${fileSizeData.sizeInKB} MB",
            modifier = Modifier.padding(PaddingValues(0.dp, 8.dp)),
            fontFamily = FontFamily.Monospace,
            fontWeight = FontWeight.Medium
        )
    }
}

private fun createSlices(fileSizeData: FileData): List<PieChartData.Slice> {
    return if (fileSizeData.children == null) {
        listOf(
            PieChartData.Slice(
                fileSizeData.sizeInKB.toFloat(),
                Color(Random.nextInt(0, 255), Random.nextInt(0, 255), Random.nextInt(0, 255), 255)
            )
        )
    } else {
        fileSizeData.children?.map { fileData ->
            PieChartData.Slice(
                fileData.sizeInKB.toFloat(),
                Color(Random.nextInt(0, 255), Random.nextInt(0, 255), Random.nextInt(0, 255), 255)
            )
        } ?: listOf()
    }
}