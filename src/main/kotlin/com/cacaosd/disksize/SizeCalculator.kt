package com.cacaosd.disksize

import java.nio.file.Files
import java.nio.file.attribute.PosixFileAttributes
import java.nio.file.attribute.PosixFilePermission
import java.nio.file.attribute.PosixFilePermissions
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path
import kotlin.io.path.isDirectory

@OptIn(ExperimentalPathApi::class)
fun main() {

    val readAttributes1 =
        Files.readAttributes(Path("/etc/polkit-1/localauthority.conf.d"), PosixFileAttributes::class.java)
    val readAttributes2 = Files.readAttributes(Path("/etc/polkit-1/localauthority"), PosixFileAttributes::class.java)

    println(readAttributes1.group())
    println(readAttributes1.owner())
    println(readAttributes1.permissions())
    println(PosixFilePermissions.toString(readAttributes1.permissions()))

    println("--------------------")
    println(readAttributes2.group())
    println(readAttributes2.owner())
    println(readAttributes2.permissions())
    println(PosixFilePermissions.toString(readAttributes2.permissions()))
    println(readAttributes2.permissions().contains(PosixFilePermission.OWNER_EXECUTE))

    val sum =
        Files.walk(Path("/"), 1)
            .filter { path ->
                val attrs = Files.readAttributes(path, PosixFileAttributes::class.java)
                attrs.permissions().contains(PosixFilePermission.GROUP_READ)
            }
            .mapToLong {
                println("dsfdsfds")
                if (it.isDirectory()) {
                    println("$it -> Directory")
                } else {
                    println("$it -> File")
                }

                Files.size(it)

            }.sum()





    println("Total Size: ${sum.div(1024)} KB")

}