package com.cacaosd.disksize.repository

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import kotlin.io.path.ExperimentalPathApi

@ExperimentalPathApi
class FileDataViewModel constructor(fileSizeRepository: FileSizeRepository) {

    private var _rootFileData: MutableState<FileData> = mutableStateOf(fileSizeRepository.getRootData()[0])
    val currentFile: State<FileData> get() = _rootFileData

    private val fileStack = ArrayDeque<FileData>()

    init {
        fileStack.add(currentFile.value)
    }

    fun addNextFile(fileData: FileData) {
        fileStack.add(fileData)
        _rootFileData.value = fileData

    }

    fun backPreviousFile() {
        if (fileStack.size > 1) {
            fileStack.removeLast()
            _rootFileData.value = fileStack.last()
        }
    }

    fun isPreviousFileExist(): Boolean = fileStack.size > 1

}