package com.cacaosd.disksize.repository

data class FileData(val name: String, val sizeInKB: Long, var children: List<FileData>? = null)
