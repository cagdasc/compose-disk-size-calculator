package com.cacaosd.disksize.repository

import java.nio.file.Files
import java.nio.file.attribute.PosixFileAttributes
import java.nio.file.attribute.PosixFilePermission
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path
import kotlin.io.path.isDirectory

@ExperimentalPathApi
class FileSizeRepository {

    fun getFileSystemSize(): FileData =
        Files.walk(Path("/"), 1)
            .filter { path ->
                val attrs = Files.readAttributes(path, PosixFileAttributes::class.java)
                attrs.permissions().contains(PosixFilePermission.GROUP_READ)
            }
            .mapToLong {
                if (it.isDirectory()) {
                    println("$it -> Directory")
                } else {
                    println("$it -> File")
                }

                Files.size(it)

            }.sum().let {
                FileData("/", it)
            }

    fun getRootData(): List<FileData> = listOf(
        FileData(
            "/", 2500, listOf(
                FileData(
                    "/user", 300,
                    listOf(
                        FileData("f1", 200),
                        FileData("f2", 300),
                        FileData("f3", 400),
                    )
                ),
                FileData(
                    "/var", 300, listOf(
                        FileData("f11", 200),
                        FileData("f12", 300),
                        FileData("f13", 400),
                    )
                ),
                FileData("/bin", 300),
                FileData("/sbin", 300),
            )
        )
    )

}